const axios = require("axios");
const cheerio = require("cheerio");

const getHtml = async () => {
  try {
    return await axios.get(
      `https://www.coupang.com/np/search?component=&q=${encodeURIComponent(
        "텀블러"
      )}&channel=user`
    );
  } catch (e) {
    console.error(e);
  }
};

const getReview = async (id) => {
  try {
    return await axios.get(
      `https://www.coupang.com/vp/product/reviews?productId=${id}&page=1&size=100&sortBy=ORDER_SCORE_ASC&ratings=&q=&viRoleCode=2&ratingSummary=true`
    );
  } catch (e) {
    console.error(e);
  }
};

getHtml()
  .then((html) => {
    const $ = cheerio.load(html.data);
    const $bodyList = $("ul.search-product-list").children();
    let object = new Map();
    $bodyList.each(function (i, elem) {
      object.set(
        $(this).find("div.name").text(),
        +$(this).find("a.search-product-link").attr("data-product-id")
      );
    });

    return object;
  })
  .then(async (object) => {
    let reviews = {};
    for (const [name, id] of object.entries()) {
      await getReview(id).then((html) => {
        const $ = cheerio.load(html.data);
        const $bodyList = $("body").children();
        reviews[id] = [];
        $bodyList.each(function (i, elem) {
          const review = $(this)
            .find("div.js_reviewArticleContent")
            .text()
            .trim();
          if (review.length > 0) reviews[id].push(review);
        });
      });
    }
    return reviews;
  })
  .then((reviews) => {
    console.log(`상품 ${Object.keys(reviews).length}개 로드`);
    Object.keys(reviews).forEach(function (key) {
      console.log(
        `상품 번호 : ${key} 리뷰 ${Object.keys(reviews[key]).length}개 로드`
      );
      reviews[key].forEach(function (review) {
        console.log(review);
      });
    });
  });
